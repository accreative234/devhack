import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountComponent } from './components/account/account.component';
import { DocsComponent } from './components/docs/docs.component';
import { LogoutComponent } from './components/logout/logout.component';
import { OverviewComponent } from './components/overview/overview.component';
import { ProductComponent } from './components/product/product.component';
import { ProjectComponent } from './components/project/project.component';
import { ShopComponent } from './components/shop/shop.component';
import { MainComponent } from './layout/main/main.component';
import { RegisterComponent } from './layout/register/register.component';
import { SigninComponent } from './layout/signin/signin.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      { path: 'overview', component: OverviewComponent },
      { path: 'shop', component: ShopComponent },
      { path: 'product', component: ProductComponent },
      { path: 'project', component: ProjectComponent },
      { path: 'account', component: AccountComponent },
      { path: 'docs', component: DocsComponent },
      { path: 'logout', component: LogoutComponent },
    ],
  },
  { path: 'login', component: SigninComponent },
  { path: 'signup', component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
