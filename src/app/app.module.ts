import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './layout/main/main.component';
import { SigninComponent } from './layout/signin/signin.component';
import { NavComponent } from './components/nav/nav.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { OverviewComponent } from './components/overview/overview.component';
import { ShopComponent } from './components/shop/shop.component';
import { ProductComponent } from './components/product/product.component';
import { ProjectComponent } from './components/project/project.component';
import { AccountComponent } from './components/account/account.component';
import { DocsComponent } from './components/docs/docs.component';
import { LogoutComponent } from './components/logout/logout.component';
import { RegisterComponent } from './layout/register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    SigninComponent,
    NavComponent,
    SidebarComponent,
    OverviewComponent,
    ShopComponent,
    ProductComponent,
    ProjectComponent,
    AccountComponent,
    DocsComponent,
    LogoutComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
